﻿namespace WindowsFormsApplication1
{
    partial class MapControlExampleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.mapControl2 = new MappingControl.MapControl();
            this.mapControl1 = new MappingControl.MapControl();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // mapControl2
            // 
            this.mapControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mapControl2.Lat = 55.598843060320675D;
            this.mapControl2.Lng = -2.6177368164062464D;
            this.mapControl2.Location = new System.Drawing.Point(475, 402);
            this.mapControl2.MapZoom = 8D;
            this.mapControl2.Name = "mapControl2";
            this.mapControl2.Size = new System.Drawing.Size(111, 117);
            this.mapControl2.StreetViewHeading = double.NaN;
            this.mapControl2.StreetViewPano = "";
            this.mapControl2.StreetViewPitch = double.NaN;
            this.mapControl2.StreetViewZoom = double.NaN;
            this.mapControl2.TabIndex = 2;
            // 
            // mapControl1
            // 
            this.mapControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mapControl1.Lat = 55.022049184439751D;
            this.mapControl1.Lng = -1.3488159179687465D;
            this.mapControl1.Location = new System.Drawing.Point(13, 33);
            this.mapControl1.MapBounds = null;
            this.mapControl1.MapMode = false;
            this.mapControl1.MapType = null;
            this.mapControl1.MapZoom = double.NaN;
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(573, 486);
            this.mapControl1.StreetViewHeading = 0D;
            this.mapControl1.StreetViewPano = null;
            this.mapControl1.StreetViewPitch = 2D;
            this.mapControl1.StreetViewZoom = 0D;
            this.mapControl1.TabIndex = 0;
            // 
            // MapControlExampleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 531);
            this.Controls.Add(this.mapControl2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mapControl1);
            this.Name = "MapControlExampleForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MappingControl.MapControl mapControl1;
        private System.Windows.Forms.Label label1;
        private MappingControl.MapControl mapControl2;
    }
}

