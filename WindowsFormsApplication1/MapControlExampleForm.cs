﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MappingControl;

namespace WindowsFormsApplication1
{
    public partial class MapControlExampleForm : Form
    {
        private Timer timer1;
        LatLng loc;

        public MapControlExampleForm()
        {
            InitializeComponent();

            timer1 = new Timer();
            timer1.Interval = 1000;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Start();
            loc = new LatLng(54.946643,-1.6427991);
            MappingControl.MapControl.StreetViewOptions svOpt = new MappingControl.MapControl.StreetViewOptions();
            svOpt.panControl=false;
            svOpt.zoomControl=false;
            svOpt.addressControl=false;
            svOpt.linksControl=false;
            mapControl1.SetStreetViewOptions(svOpt);
            mapControl1.LatLng = loc;
            mapControl1.StreetViewHeading = 245;
            mapControl2.MapType = "HYBRID";
            mapControl2.LatLng = loc;
            mapControl2.MapZoom = 100;
            //mapControl1.MapMode = false;
            mapControl2.MarkerShown = true;

            mapControl2.AddressReady += new MappingControl.MapControl.AddressReadyEventHandler(mapControl1_AddressReady);
            mapControl1.OnError += new MappingControl.MapControl.OnErrorEventHandler(mapControl1_OnError);
            mapControl2.OnError += new MappingControl.MapControl.OnErrorEventHandler(mapControl1_OnError);
            mapControl1.ViewPositionChanged += new MapControl.ViewPositionChangedEventHandler(mapControl1_ViewPositionChanged);
        }

        void mapControl1_ViewPositionChanged(object sender, MapControl.ViewPositionChangedEventArgs e)
        {
            mapControl2.requestAddress();
        }

        void mapControl1_OnError(object sender, MappingControl.MapControl.OnErrorEventArgs e)
        {
            label1.BeginInvoke(new setLabelDelegate(setLabel), new object[] { e.ErrorString });
            System.Diagnostics.Debug.WriteLine(e.ErrorString);
        }

        void mapControl1_AddressReady(object sender, MappingControl.MapControl.AddressReadyEventArgs e)
        {
            label1.BeginInvoke(new setLabelDelegate(setLabel), new object[] { e.Address.getNiceLocalString() });
        }

        delegate void setLabelDelegate(String s);

        void setLabel(String s)
        {
            label1.Text = s;
        }

        int mode = 4;
        int heading = 0;
        int pitch = 0;
        double zoom = 0;
        void timer1_Tick(object sender, EventArgs e)
        {
            if (mode == -1)
            {
                zoom += 0.1;
                mapControl1.StreetViewZoom = zoom;
                if (zoom > 3) mode++;
            }
            else if (mode == 0)
            {
                zoom -= 0.5;
                mapControl1.StreetViewZoom = zoom;
                if (zoom < 1) mode++;
            }
            else if (mode == 1)
            {
                heading+=5;
                mapControl1.StreetViewHeading = heading;
                if (heading == 270) mode++;
            }
            else if (mode == 2)
            {
                pitch+=5;
                mapControl1.StreetViewPitch = pitch;
                if (pitch == 45) mode++;
            }
            else if (mode == 3)
            {
                pitch-=5;
                mapControl1.StreetViewPitch = pitch;
                if (pitch == 0) mode++;
            }
            else if (mode == 4)
            {
                mapControl1.LatLng = loc;
                mapControl2.MapBounds = new RectangularBounds(loc.Lat - 0.0001, loc.Lat + 0.0001, loc.Lng - 0.0001, loc.Lng + 0.0001);
                loc = new LatLng(loc.Lat, loc.Lng - 0.0001);
                mapControl2.MarkerLatLng = mapControl1.LatLng;
            }
        }
    }
}
