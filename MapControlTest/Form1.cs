﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapControlTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            mapControl1.AddressReady += new MappingControl.MapControl.AddressReadyEventHandler(mapControl1_AddressReady);
            mapControl1.MapBoundsChanged += new MappingControl.MapControl.MapBoundsChangedEventHandler(mapControl1_MapBoundsChanged);
            mapControl1.MapCenterChanged += new MappingControl.MapControl.MapCenterChangedEventHandler(mapControl1_MapCenterChanged);
            mapControl1.MapLoaded += new MappingControl.MapControl.MapLoadedEventHandler(mapControl1_MapLoaded);
            mapControl1.MapTiltChanged += new MappingControl.MapControl.MapTiltChangedEventHandler(mapControl1_MapTiltChanged);
            mapControl1.MapZoomChanged += new MappingControl.MapControl.MapZoomChangedEventHandler(mapControl1_MapZoomChanged);
            mapControl1.OnError += new MappingControl.MapControl.OnErrorEventHandler(mapControl1_OnError);
            mapControl1.ViewLinksChanged += new MappingControl.MapControl.ViewLinksChangedEventHandler(mapControl1_ViewLinksChanged);
            mapControl1.ViewPanoChanged += new MappingControl.MapControl.ViewPanoChangedEventHandler(mapControl1_ViewPanoChanged);
            mapControl1.ViewPositionChanged += new MappingControl.MapControl.ViewPositionChangedEventHandler(mapControl1_ViewPositionChanged);
            mapControl1.ViewPOVChanged += new MappingControl.MapControl.ViewPOVChangedEventHandler(mapControl1_ViewPOVChanged);
            mapControl1.ViewStatusChanged += new MappingControl.MapControl.ViewStatusChangedEventHandler(mapControl1_ViewStatusChanged);
            mapControl1.ViewZoomChanged += new MappingControl.MapControl.ViewZoomChangedEventHandler(mapControl1_ViewZoomChanged);
        }

        void mapControl1_ViewZoomChanged(object sender, MappingControl.MapControl.ViewZoomChangedEventArgs e)
        {
            log("View Zoom Changed: " + e.Zoom);
        }

        void mapControl1_ViewStatusChanged(object sender, MappingControl.MapControl.ViewStatusChangedEventArgs e)
        {
            log("View Status Changed: " + e.ViewStatus);
        }

        void mapControl1_ViewPOVChanged(object sender, MappingControl.MapControl.ViewPOVChangedEventArgs e)
        {
            log("View POV Changed: Heading:" + e.Heading + " Pitch: " + e.Pitch);
        }

        void mapControl1_ViewPositionChanged(object sender, MappingControl.MapControl.ViewPositionChangedEventArgs e)
        {
            log("View Position Changed: " + e.LatLng);
        }

        void mapControl1_ViewPanoChanged(object sender, MappingControl.MapControl.ViewPanoChangedEventArgs e)
        {
            log("View Pano Changed: " + e.Pano);
        }

        void mapControl1_ViewLinksChanged(object sender, MappingControl.MapControl.ViewLinksChangedEventArgs e)
        {
            String s = "View Links Changed: ";
            foreach(MappingControl.MapControl.StreetViewLink svl in e.Links)
            {
                s+="("+svl.Heading + ", "+svl.Description+", "+svl.Panorama+") ";
            }
            log("View Links Changed: " + s);
        }

        void mapControl1_OnError(object sender, MappingControl.MapControl.OnErrorEventArgs e)
        {
            log("Error: " + e.ErrorString);
        }

        void mapControl1_MapZoomChanged(object sender, MappingControl.MapControl.MapZoomChangedEventArgs e)
        {
            log("Map Zoom Changed: " + e.Zoom);
        }

        void mapControl1_MapTiltChanged(object sender, MappingControl.MapControl.MapTiltChangedEventArgs e)
        {
            log("Map Tilt Changed: " + e.Tilt);
        }

        void mapControl1_MapLoaded(object sender, EventArgs e)
        {
            log("Map Loaded");
        }

        void mapControl1_MapCenterChanged(object sender, MappingControl.MapControl.MapCenterChangedEventArgs e)
        {
            log("Map Center Changed: "+e.LatLng);
        }

        void mapControl1_MapBoundsChanged(object sender, MappingControl.MapControl.MapBoundsChangedEventArgs e)
        {
            log("Map Bounds Changed: " + e.Bounds);
        }

        void log(String s)
        {
            textBox1.Text = s + Environment.NewLine + textBox1.Text;
        }

        void mapControl1_AddressReady(object sender, MappingControl.MapControl.AddressReadyEventArgs e)
        {
            log("Address Ready: " + e.Address.AddressString);
        }
    }
}
