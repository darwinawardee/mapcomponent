﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MappingControl
{
    public partial class MapControl : UserControl
    {
        //TODO: Find way to auto generate this
        private String versionString = "V14_02_04_1200";

        public enum LoggingFlags
        {
            NONE = 0,
            ERRORS = 1, //TODO!
            EVENTS = 2, //TODO!
            PROPERTIES = 4, //TODO!
            STRICT_ERRORS = 8,
            ALL_EXCEPT_STRICT = ERRORS + EVENTS + PROPERTIES,
            ALL = ERRORS + EVENTS + PROPERTIES + STRICT_ERRORS
        }

        private bool checkLogLevel(LoggingFlags requiredLevel)
        {
            return (requiredLevel & logLevel) > LoggingFlags.NONE;
        }

        private void log(String message)
        {
            System.Diagnostics.Debug.Print("MapControl:"+message);
        }

        private void log(String message, LoggingFlags requiredLevel)
        {
            if (checkLogLevel(requiredLevel))
            {
                System.Diagnostics.Debug.Print("MapControl:" + message);
            }
        }

        /*
         * Private variables, properties expose these publicly
         */
        private bool mapMode, loaded;
        private LatLng latLng;
        private double mapZoom, mapTilt;
        private double viewZoom, viewHeading, viewPitch, viewSnapRadius;
        private String mapType;
        private bool markerShown;
        private LatLng markerLatLng;
        private String viewStatus, lastError, viewPano, viewLinks;
        private GeocoderAddress address;
        private StreetViewLink[] links;
        private RectangularBounds mapBounds;

        public LoggingFlags logLevel = LoggingFlags.ERRORS;

        private MapScriptInterop mapScriptInterop;

        private HtmlElementErrorEventHandler windowError;
        private HtmlElementEventHandler mouseOver;
        private HtmlElementEventHandler mouseDown;
        private HtmlElementEventHandler mouseUp;
        private HtmlElementEventHandler click;

        /*
         * Called when MapControl initialises
         */
        public MapControl()
        {
            latLng = new LatLng(0, 0);

            InitializeComponent();

            //the following code sets the default values for properties
            foreach (System.ComponentModel.PropertyDescriptor property in System.ComponentModel.TypeDescriptor.GetProperties(this))
            {
                System.ComponentModel.DefaultValueAttribute myAttribute = (System.ComponentModel.DefaultValueAttribute)property.Attributes[typeof(System.ComponentModel.DefaultValueAttribute)];

                if (myAttribute != null)
                {
                    property.SetValue(this, myAttribute.Value);
                }
            }

            browser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(browser_DocumentCompleted);
            windowError = new HtmlElementErrorEventHandler(Window_Error);
            mouseOver = new HtmlElementEventHandler(Document_MouseOver);
            mouseDown = new HtmlElementEventHandler(Document_MouseDown);
            mouseUp = new HtmlElementEventHandler(Document_MouseUp);
            click = new HtmlElementEventHandler(Document_Click);
        }

        

        public void requestAddress()
        {
            mapScriptInterop.requestAddress(latLng);
        }

        void addDocumentListeners()
        {
            browser.Document.Window.Error += windowError;
            browser.Document.MouseOver += mouseOver;
            browser.Document.MouseDown += mouseDown;
            browser.Document.MouseUp += mouseUp;
            browser.Document.Click += click;
        }

        void removeDocumentListeners()
        {
            browser.Document.Window.Error -= windowError;
            browser.Document.MouseOver -= mouseOver;
            browser.Document.MouseDown -= mouseDown;
            browser.Document.MouseUp -= mouseUp;
            browser.Document.Click -= click;
        }

        void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            addDocumentListeners();
            //((WebBrowser)sender).Document.Window.Error += new HtmlElementErrorEventHandler(Window_Error);
            //((WebBrowser)sender).Document.MouseOver += new HtmlElementEventHandler(Document_MouseOver);
        }

        void Document_MouseOver(object sender, HtmlElementEventArgs e)
        {
            if (MouseOver != null)
            {
                MouseOver(sender, e);
            }
        }

        void Document_MouseDown(object sender, HtmlElementEventArgs e)
        {
            if (MouseDown != null)
            {
                MouseDown(sender, e);
            }
        }

        void Document_MouseUp(object sender, HtmlElementEventArgs e)
        {
            if (MouseUp != null)
            {
                MouseUp(sender, e);
            }
        }

        void Document_Click(object sender, HtmlElementEventArgs e)
        {
            if (Click != null)
            {
                Click(sender, e);
            }
        }

        public void forceReset()
        {
            log("Forced reset", LoggingFlags.ERRORS);
            Window_Error(null, null);
        }

        //reset
        void Window_Error(object sender, HtmlElementErrorEventArgs e)
        {
            log("Resetting on Window_Error", LoggingFlags.ERRORS);
            if (e != null)
            {
                lastError = DateTime.Now.ToShortTimeString() + e.Description;

                if (OnError != null)
                {
                    OnError(this, new OnErrorEventArgs(lastError));
                }

                e.Handled = true;
            }

            if (mapMode)
            {
                showMap();
            }
            else
            {
                showStreetview(svOpts);
            }

            
        }

        /*
         * Called when maps itself finishing loads
         */
        private void MapControl_Load(object sender, EventArgs e)
        {
            mapScriptInterop = new MapScriptInterop(this);
            browser.ObjectForScripting = mapScriptInterop;
        }

        /*
         * Show a map
         */
        public void showMap()
        {
            removeDocumentListeners();
            mapMode = true;
            loaded = false;
            browser.Navigate("about:blank");
            HtmlDocument doc = this.browser.Document;
            doc.OpenNew(false);
            doc.Write(String.Empty);
            browser.DocumentText = Properties.Resources.MapPage;
        }

        /*
         * Show a map at a location
         */
        public void showMap(LatLng latLng, double zoom)
        {
            showMap();
            this.latLng = latLng;
            this.mapZoom = zoom;
        }

        public void SetStreetViewOptions(StreetViewOptions svo)
        {
            svOpts = svo;
            showStreetview(svo);
        }

        private StreetViewOptions svOpts = new StreetViewOptions();
        public class StreetViewOptions
        {
            public StreetViewOptions() { }
            public bool? addressControl;
            public bool? clickToGo;
            public bool? disableDefaultUI;
            public bool? disableDoubleClickZoom;
            public bool? imageDateControl;
            public bool? linksControl;
            public bool? panControl;
            public bool? scrollwheel;
            public bool? zoomControl;

            private String indent = "              ";
            
            private String boolToString(String name, bool? val)
            {
                if (val.HasValue)
                {
                    return indent + name + ": " + (val.Value?"true":"false") + ",\n";
                }
                return "";
            }
            public String generateString(double lat,double lng,double heading, double pitch)
            {
                String newOptionText = indent + "position: new google.maps.LatLng(" + lat + ", " + lng + ")," + "\n";
                newOptionText += boolToString("addressControl", addressControl);
                newOptionText += boolToString("clickToGo", clickToGo);
                newOptionText += boolToString("disableDefaultUI", disableDefaultUI);
                newOptionText += boolToString("disableDoubleClickZoom", disableDoubleClickZoom);
                newOptionText += boolToString("imageDateControl", imageDateControl);
                newOptionText += boolToString("linksControl", linksControl);
                newOptionText += boolToString("panControl", panControl);
                newOptionText += boolToString("scrollwheel", scrollwheel);
                newOptionText += boolToString("zoomControl", zoomControl);
                newOptionText += indent + "pov: {" + "\n";
                newOptionText += indent + "    heading: " + heading + "," + "\n";
                newOptionText += indent + "    pitch: " + pitch + "\n";
                newOptionText += indent + "}";
                return newOptionText; 
            }
        }

        /*
         * Show streetview
         */
        public void showStreetview(StreetViewOptions svo)
        {
            removeDocumentListeners();
            mapMode = false;
            loaded = false;
            browser.Navigate("about:blank");
            HtmlDocument doc = this.browser.Document;
            doc.OpenNew(false);
            doc.Write(String.Empty);

            String streetviewPageHtml = Properties.Resources.StreetviewPage;
            String startString = "var panoramaOptions = {";
            int optionsStart = streetviewPageHtml.IndexOf(startString) + startString.Length;
            int optionsEnd = streetviewPageHtml.IndexOf("};", optionsStart);
            String newHtml = streetviewPageHtml.Substring(0, optionsStart);
            newHtml += "\n"+svo.generateString(Lat,Lng,viewHeading,viewPitch);
            newHtml += streetviewPageHtml.Substring(optionsEnd);
            browser.DocumentText = newHtml;
        }

        /*
         * Called when the map is ready to receive locations etc
         */
        private void initMap()
        {
            loaded = true;
            if (mapMode)
            {
                mapScriptInterop.panMapTo(latLng);
                mapScriptInterop.setMapZoom(mapZoom);
                mapScriptInterop.setMapType(mapType);
                mapScriptInterop.showMarker(markerShown);
                mapScriptInterop.moveMarker(markerLatLng);
            }
            else
            {
                mapScriptInterop.setStreetViewPosition(latLng, viewSnapRadius);
                mapScriptInterop.setStreetViewPOV(viewHeading, viewPitch);
                mapScriptInterop.setStreetViewZoom(viewZoom);
            }
        }
    }
}
