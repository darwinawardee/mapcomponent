﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public interface Bounds
    {
        bool isInside(LatLng loc);
    }
}
