﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public partial class MapControl : System.Windows.Forms.UserControl
    {
        public delegate void MapLoadedEventHandler(object sender, EventArgs e);

        [System.ComponentModel.Description("On completion of map initialisation")]
        public event MapLoadedEventHandler MapLoaded;

        public delegate void MapCenterChangedEventHandler(object sender, MapCenterChangedEventArgs e);

        [System.ComponentModel.Description("On change of map centre")]
        public event MapCenterChangedEventHandler MapCenterChanged;

        public class MapCenterChangedEventArgs : EventArgs
        {
            public MapCenterChangedEventArgs(LatLng latLng)
            {
                this.latLng = latLng;
            }

            private LatLng latLng;

            public LatLng LatLng
            {
                get
                {
                    return latLng;
                }
            }
        }

        public delegate void MapTypeChangedEventHandler(object sender, MapTypeChangedEventArgs e);

        [System.ComponentModel.Description("On change of map type")]
        public event MapTypeChangedEventHandler MapTypeChanged;

        public class MapTypeChangedEventArgs : EventArgs
        {
            public MapTypeChangedEventArgs(String type)
            {
                this.type = type;
            }

            private String type;

            public String Type
            {
                get
                {
                    return type;
                }
            }
        }

        public delegate void MapZoomChangedEventHandler(object sender, MapZoomChangedEventArgs e);

        [System.ComponentModel.Description("On change of map zoom")]
        public event MapZoomChangedEventHandler MapZoomChanged;

        public class MapZoomChangedEventArgs : EventArgs
        {
            public MapZoomChangedEventArgs(double zoom)
            {
                this.zoom = zoom;
            }

            private double zoom;

            public double Zoom
            {
                get
                {
                    return zoom;
                }
            }
        }

        public delegate void MapTiltChangedEventHandler(object sender, MapTiltChangedEventArgs e);

        [System.ComponentModel.Description("On change of map tilt")]
        public event MapTiltChangedEventHandler MapTiltChanged;

        public class MapTiltChangedEventArgs : EventArgs
        {
            public MapTiltChangedEventArgs(double tilt)
            {
                this.tilt = tilt;
            }

            private double tilt;

            public double Tilt
            {
                get
                {
                    return tilt;
                }
            }
        }

        public delegate void MapBoundsChangedEventHandler(object sender, MapBoundsChangedEventArgs e);

        [System.ComponentModel.Description("On change of map bounds")]
        public event MapBoundsChangedEventHandler MapBoundsChanged;

        public class MapBoundsChangedEventArgs : EventArgs
        {
            public MapBoundsChangedEventArgs(RectangularBounds bounds)
            {
                this.bounds = bounds;
            }

            private RectangularBounds bounds;

            public RectangularBounds Bounds
            {
                get
                {
                    return bounds;
                }
            }
        }

        public delegate void ViewPositionChangedEventHandler(object sender, ViewPositionChangedEventArgs e);

        [System.ComponentModel.Description("On change of street view position")]
        public event ViewPositionChangedEventHandler ViewPositionChanged;

        public class ViewPositionChangedEventArgs : EventArgs
        {
            public ViewPositionChangedEventArgs(LatLng latLng)
            {
                this.latLng = latLng;
            }

            private LatLng latLng;

            public LatLng LatLng
            {
                get
                {
                    return latLng;
                }
            }
        }

        public delegate void ViewZoomChangedEventHandler(object sender, ViewZoomChangedEventArgs e);

        [System.ComponentModel.Description("On change of street view zoom")]
        public event ViewZoomChangedEventHandler ViewZoomChanged;

        public class ViewZoomChangedEventArgs : EventArgs
        {
            public ViewZoomChangedEventArgs(double zoom)
            {
                this.zoom = zoom;
            }

            private double zoom;

            public double Zoom
            {
                get
                {
                    return zoom;
                }
            }
        }

        public delegate void ViewPOVChangedEventHandler(object sender, ViewPOVChangedEventArgs e);

        [System.ComponentModel.Description("On change of street view POV")]
        public event ViewPOVChangedEventHandler ViewPOVChanged;

        public class ViewPOVChangedEventArgs : EventArgs
        {
            public ViewPOVChangedEventArgs(double heading, double pitch)
            {
                this.heading = heading;
                this.pitch = pitch;
            }

            private double heading;
            private double pitch;

            public double Heading
            {
                get
                {
                    return heading;
                }
            }

            public double Pitch
            {
                get
                {
                    return pitch;
                }
            }
        }

        public delegate void ViewStatusChangedEventHandler(object sender, ViewStatusChangedEventArgs e);

        [System.ComponentModel.Description("On change of street view status")]
        public event ViewStatusChangedEventHandler ViewStatusChanged;

        public class ViewStatusChangedEventArgs : EventArgs
        {
            public ViewStatusChangedEventArgs(String viewStatus)
            {
                this.viewStatus = viewStatus;
            }

            private String viewStatus;

            public String ViewStatus
            {
                get
                {
                    return viewStatus;
                }
            }
        }

        public delegate void OnErrorEventHandler(object sender, OnErrorEventArgs e);

        [System.ComponentModel.Description("On javascript error")]
        public event OnErrorEventHandler OnError;

        public class OnErrorEventArgs : EventArgs
        {
            public OnErrorEventArgs(String errorString)
            {
                this.errorString = errorString;
            }

            private String errorString;

            public String ErrorString
            {
                get
                {
                    return errorString;
                }
            }
        }

        public delegate void ViewPanoChangedEventHandler(object sender, ViewPanoChangedEventArgs e);

        [System.ComponentModel.Description("On panorama change")]
        public event ViewPanoChangedEventHandler ViewPanoChanged;

        public class ViewPanoChangedEventArgs : EventArgs
        {
            public ViewPanoChangedEventArgs(String pano)
            {
                this.pano = pano;
            }

            private String pano;

            public String Pano
            {
                get
                {
                    return pano;
                }
            }
        }

        public delegate void ViewLinksChangedEventHandler(object sender, ViewLinksChangedEventArgs e);

        [System.ComponentModel.Description("On panorama links change")]
        public event ViewLinksChangedEventHandler ViewLinksChanged;

        public class ViewLinksChangedEventArgs : EventArgs
        {
            public ViewLinksChangedEventArgs(LatLng loc, StreetViewLink[] links)
            {
                location = loc;
                this.links = links;
            }

            private LatLng location;
            private StreetViewLink[] links;

            public LatLng LatLng
            {
                get
                {
                    return location;
                }
            }

            public StreetViewLink[] Links
            {
                get
                {
                    return links;
                }
            }
        }

        public delegate void AddressReadyEventHandler(object sender, AddressReadyEventArgs e);

        [System.ComponentModel.Description("On return of results from RequestAddress")]
        public event AddressReadyEventHandler AddressReady;

        public class AddressReadyEventArgs : EventArgs
        {
            public AddressReadyEventArgs(GeocoderAddress address)
            {
                this.address = address;
            }

            private GeocoderAddress address;

            public GeocoderAddress Address
            {
                get
                {
                    return address;
                }
            }
        }

        public delegate void DocumentMouseEventHandler(object sender, System.Windows.Forms.HtmlElementEventArgs e);

        public event DocumentMouseEventHandler MouseOver;
        public new event DocumentMouseEventHandler MouseDown;
        public new event DocumentMouseEventHandler MouseUp;
        public new event DocumentMouseEventHandler Click;
    }
}
