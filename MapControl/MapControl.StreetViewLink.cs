﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public partial class MapControl : System.Windows.Forms.UserControl
    {
        public class StreetViewLink
        {
            private String description, pano;
            private int heading;

            private StreetViewLink(String description, int heading, String pano)
            {
                this.description = description;
                this.heading = heading;
                this.pano = pano;
            }

            public String Description
            {
                get
                {
                    return description;
                }
            }

            public String Panorama
            {
                get
                {
                    return pano;
                }
            }

            public int Heading
            {
                get
                {
                    return heading;
                }
            }

            public static StreetViewLink[] ParseStreetViewLinks(String linksString)
            {
                //return new StreetViewLink[0];
                
                String[] links = linksString.Split(new char[] { ';' });
                StreetViewLink[] result = new StreetViewLink[links.Length - 1];
                for (int i = 0; i < result.Length; i++)
                {
                    String[] parts = links[i].Split(new char[] { ',' });
                    result[i] = new StreetViewLink(parts[0], (int)float.Parse(parts[1]), parts[2]);
                }
                return result;
            }
        }
    }
}
