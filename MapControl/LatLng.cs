﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public class LatLng
    {
        private double lat, lng;

        public LatLng(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
        }

        public double Lat
        {
            get
            {
                return lat;
            }
        }

        public double Lng
        {
            get
            {
                return lng;
            }
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 31 + lat.GetHashCode();
                hash = hash * 31 + lng.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(System.Object obj)
        {
            double threshold = 0.000000001;
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            LatLng p = obj as LatLng;
            if ((System.Object)p == null)
            {
                return false;
            }

            double deltaLat = Math.Abs(lat - p.lat);
            if (deltaLat > threshold) return false;
            double deltaLng = Math.Abs(lng - p.lng);
            return (deltaLng <= threshold);
        }

        public override string ToString()
        {
            return "LatLng(" + lat + "," + lng + ")";
        }

        public static double distanceBetween(LatLng a, LatLng b)
        {
            double radius = 6371000; //m
            double dLat = toRadian(a.Lat - b.Lat);
            double dLon = toRadian(a.Lng - b.Lng);
            double insideFormula = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Cos(toRadian(b.Lat)) * Math.Cos(toRadian(a.Lat)) *
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt(insideFormula)));
        }

        private static double toRadian(double val)
        {
            return (Math.PI / 180) * val;
        } 
    }
}
