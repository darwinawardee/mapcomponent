﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public class CircularBounds : Bounds
    {
        private LatLng centre;
        private double radiusOfAcceptance;
        //private RectangularBounds quickTest;
        //Earth max radius = 6.384x10^6 m
        // x2 xPI
        //max circumference = 4.0111855 x 10^7 m
        // /360
        //max meters per degree lat or lng = 111421
        //private const double maxMeteresPerDegree = 111422;

        public CircularBounds(LatLng centre, double radiusOfAcceptance)
        {
            this.centre = centre;
            this.radiusOfAcceptance = radiusOfAcceptance;
            /*double halfMaxDegreesOfAcceptance = radiusOfAcceptance / (2*maxMeteresPerDegree);
            quickTest = new RectangularBounds(
                centre.Lat - halfMaxDegreesOfAcceptance,
                centre.Lat + halfMaxDegreesOfAcceptance,
                centre.Lng - halfMaxDegreesOfAcceptance,
                centre.Lng + halfMaxDegreesOfAcceptance);*/

        }

        public bool isInside(LatLng loc)
        {
            //if (!quickTest.isInside(loc)) return false;
            return distanceFrom(loc) <= radiusOfAcceptance;
        }

        private LatLng lastDistanceCheck;
        private double lastValue;
        public double distanceFrom(LatLng loc)
        {
            if (!loc.Equals(lastDistanceCheck))
            {
                lastDistanceCheck = loc;
                lastValue = LatLng.distanceBetween(loc, centre);
            }
            return lastValue;
        }

        
    }
}
