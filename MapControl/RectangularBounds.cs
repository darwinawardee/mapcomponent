﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public class RectangularBounds : Bounds
    {
        private double minLat, maxLat, minLng, maxLng;
        public RectangularBounds(double minLat, double maxLat, double minLng, double maxLng)
        {
            if (minLat < maxLat)
            {
                this.maxLat = maxLat;
                this.minLat = minLat;
            }
            else
            {
                this.maxLat = minLat;
                this.minLat = maxLat;
            }

            if (minLng < maxLng)
            {
                this.maxLng = maxLng;
                this.minLng = minLng;
            }
            else
            {
                this.maxLng = minLng;
                this.minLng = maxLng;
            }
        }

        public bool isInside(LatLng loc)
        {
            return (loc.Lat <= maxLat && loc.Lat >= minLat && loc.Lng <= maxLng && loc.Lng >= minLng);
        }

        public double MinLat{ get{ return minLat;}}
        public double MaxLat { get { return maxLat; } }
        public double MinLng { get { return minLng; } }
        public double MaxLng { get { return maxLng; } }

        public override string ToString()
        {
            return "RectangularBounds(" + minLat + " : " + maxLat +  "," + minLng + " : " + maxLng + ")";
        }
    }
}
