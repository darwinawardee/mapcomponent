﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public partial class MapControl : System.Windows.Forms.UserControl
    {
        /*
        * This class provides interop between C# and the google maps javascript code
        */
        [System.Runtime.InteropServices.ComVisible(true)]
        public class MapScriptInterop
        {
            private MapControl mapControl;

            public MapScriptInterop(MapControl form)
            {
                mapControl = form;
            }

            //Called from javascript on map center_changed event or streetview panorama position_changed
            public void MapPositionChanged(double lat, double lng)
            {
                LatLng latLng = new LatLng(lat, lng);
                mapControl.latLng = latLng;
                if (mapControl.MapCenterChanged != null)
                    mapControl.MapCenterChanged(this, new MapCenterChangedEventArgs(latLng));

            }

            //Called from javascript on map zoom_changed event
            public void MapZoomChanged(double zoom)
            {
                if (mapControl.MapZoomChanged != null)
                    mapControl.MapZoomChanged(this, new MapZoomChangedEventArgs(zoom));
                mapControl.mapZoom = zoom;
            }

            //Called from javascript on map maptypeid_changed event
            public void MapTypeChanged(String type)
            {
                if (mapControl.MapTypeChanged != null)
                    mapControl.MapTypeChanged(this, new MapTypeChangedEventArgs(type));
                mapControl.mapType = type;
            }

            //Called from javascript on map zoom_changed event
            public void MapBoundsChanged(double minLat, double maxLat, double minLng, double maxLng)
            {
                mapControl.mapBounds = new RectangularBounds(minLat, maxLat, minLng, maxLng);
                if (mapControl.MapBoundsChanged != null)
                    mapControl.MapBoundsChanged(this, new MapBoundsChangedEventArgs(mapControl.mapBounds));

            }

            //Called from javascript on map tilt_changed event
            public void TiltChanged(double tilt)
            {
                if (mapControl.MapTiltChanged != null)
                    mapControl.MapTiltChanged(this, new MapTiltChangedEventArgs(tilt));
                mapControl.mapTilt = tilt;
            }

            //Called from javascript on map center_changed event or streetview panorama position_changed
            public void ViewPositionChanged(double lat, double lng)
            {
                LatLng latLng = new LatLng(lat, lng);
                if (mapControl.ViewPositionChanged != null)
                    mapControl.ViewPositionChanged(this, new ViewPositionChangedEventArgs(latLng));
                mapControl.latLng = latLng;
            }

            //Called from javascript on map center_changed event or streetview panorama position_changed
            public void ViewStatusChanged(String viewStatus)
            {
                if (mapControl.ViewStatusChanged != null)
                    mapControl.ViewStatusChanged(this, new ViewStatusChangedEventArgs(viewStatus));
                mapControl.viewStatus = viewStatus;
            }

            //Called from javascript on streetview panorama zoom_changed event
            public void ViewZoomChanged(double zoom)
            {
                if (mapControl.ViewZoomChanged != null)
                    mapControl.ViewZoomChanged(this, new ViewZoomChangedEventArgs(zoom));
                mapControl.viewZoom = zoom;
            }

            //Called from javascript on streetview panorama zoom_changed event
            public void ViewPanoChanged(String pano)
            {
                if (mapControl.ViewPanoChanged != null)
                    mapControl.ViewPanoChanged(this, new ViewPanoChangedEventArgs(pano));
                mapControl.viewPano = pano;
            }

            //Called from javascript on streetview panorama zoom_changed event
            public void ViewLinksChanged(String linksString)
            {
                mapControl.viewLinks = linksString;
                mapControl.links = StreetViewLink.ParseStreetViewLinks(linksString);
                if (mapControl.ViewLinksChanged != null)
                    mapControl.ViewLinksChanged(this, new ViewLinksChangedEventArgs(mapControl.latLng, mapControl.links));
            }

            //Called from javascript on streetview panorama zoom_changed event
            public void ViewPOVChanged(double heading, double pitch)
            {
                if (mapControl.ViewPOVChanged != null)
                    mapControl.ViewPOVChanged(this, new ViewPOVChangedEventArgs(heading, pitch));
                mapControl.viewHeading = heading;
                mapControl.viewPitch = pitch;
            }

            //Called from javascript when initialisation function is done
            public void MapLoaded()
            {
                if (mapControl.MapLoaded != null)
                    mapControl.MapLoaded(this, EventArgs.Empty);
                mapControl.initMap();
            }

            //Called from javascript when initialisation function is done
            public void OnError(String errorString)
            {
                if (mapControl.OnError != null)
                    mapControl.OnError(this, new OnErrorEventArgs(errorString));
                mapControl.lastError = errorString;
                mapControl.log("Error in javascript:" + errorString, LoggingFlags.ERRORS);
            }

            //Called from javascript when initialisation function is done
            public void AddressReady(String addressString)
            {
                mapControl.address = new GeocoderAddress(addressString);
                if (mapControl.AddressReady != null)
                    mapControl.AddressReady(this, new AddressReadyEventArgs(mapControl.address));
            }

            //invoke a script in javascript
            private object InvokeScript(string name, params object[] args)
            {
                return mapControl.browser.Document.InvokeScript(name, args);
            }

            //pan map to location
            public void panMapTo(LatLng latLng)
            {
                if (latLng != null)
                {
                    InvokeScript("panTo", latLng.Lat, latLng.Lng);
                }
            }

            public void showMarker(bool show)
            {
                InvokeScript("showMarker", show);
            }

            public void moveMarker(LatLng latLng)
            {
                if (latLng != null)
                {
                    InvokeScript("moveMarker", latLng.Lat, latLng.Lng);
                }
            }

            //set map zoom
            public void setMapType(String type)
            {
                InvokeScript("setMapType", type);
            }

            //set map zoom
            public void setMapZoom(double zoom)
            {
                InvokeScript("setZoom", zoom);
            }

            //set map bounds
            public void setMapBounds(RectangularBounds bounds)
            {
                InvokeScript("setBounds", bounds.MinLat, bounds.MaxLat, bounds.MinLng, bounds.MaxLng);
            }

            //set streetview zoom
            public void setStreetViewZoom(double zoom)
            {
                InvokeScript("setZoom", zoom);
            }

            //set streetview POV 
            public void setStreetViewPOV(double heading, double pitch)
            {
                InvokeScript("setPov", heading, pitch);
            }

            //set streetview POV 
            public void setStreetViewPano(String pano)
            {
                InvokeScript("setPano", pano);
            }

            //set streetview position
            public void setStreetViewPosition(LatLng latLng)
            {
                InvokeScript("goto", latLng.Lat, latLng.Lng, 50);
            }

            //set streetview position
            public void setStreetViewPosition(LatLng latLng, double radius)
            {
                InvokeScript("goto", latLng.Lat, latLng.Lng, radius);
            }

            //set address
            public void requestAddress(LatLng latLng)
            {
                InvokeScript("requestAddress", latLng.Lat, latLng.Lng);
            }
        }
    }
}
