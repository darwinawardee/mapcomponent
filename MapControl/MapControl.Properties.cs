﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public partial class MapControl : System.Windows.Forms.UserControl
    {
        /// <summary>
        /// The radius within which streetview will snap to a panorama
        /// </summary>
        [System.ComponentModel.DefaultValue(50.0)]
        [System.ComponentModel.Description("The radius within which streetview will snap to a panorama")]
        public double ViewSnapRadius
        {
            get
            {
                return viewSnapRadius;
            }
            set
            {
                viewSnapRadius = value;
            }
        }



        /// <summary>
        /// If true the control is displaying a map, if false a street view.
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        [System.ComponentModel.Description("Map if true, streetview if false")]
        public Boolean MapMode
        {
            get
            {
                return mapMode;
            }
            set
            {
                if (value)
                {
                    showMap();
                }
                else
                {
                    showStreetview(svOpts);
                }
            }
        }

        /// <summary>
        /// If true the control is displaying a map, if false a street view.
        /// </summary>
        [System.ComponentModel.DefaultValue("ROADMAP")]
        [System.ComponentModel.Description("Type of google map")]
        public String MapType
        {
            get
            {
                return mapType;
            }
            set
            {
                mapType = value;
                if (mapMode && loaded)
                {
                    mapScriptInterop.setMapType(value);
                }
                else
                {
                    log("Cannot set MapType mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        /// <summary>
        /// Set the latitude of the view in decimal degrees. Valid range -90 to 90.
        /// 
        /// Note if you are changing the latitude and longitude use LatLng to avoid multiple the map loading twice.
        /// </summary>
        [System.ComponentModel.DefaultValue(55)]
        [System.ComponentModel.Description("Latitude of map or streetview")]
        public double Lat
        {
            get
            {
                return latLng.Lat;
            }
            set
            {
                if (value > 90 || value < -90 || double.IsNaN(value) || double.IsInfinity(value))
                {
                    log("Cannot set Lat, out of range " + value, LoggingFlags.ERRORS);
                    return;//throw new ArgumentOutOfRangeException("Lat", "Latitude must be between [-90,90], attempted value: " + value);
                }
                latLng = new LatLng(value, latLng.Lng);
                if (loaded)
                {
                    if (mapMode)
                    {
                        mapScriptInterop.panMapTo(latLng);
                    }
                    else
                    {
                        mapScriptInterop.setStreetViewPosition(latLng, viewSnapRadius);
                    }
                }
                else
                {
                    log("Cannot set Lat, loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        /// <summary>
        /// Set the longitude of the view in decimal degrees. Valid range 0 to 360.
        /// 
        /// Note if you are changing the latitude and longitude use LatLng to avoid multiple the map loading twice.
        /// </summary>
        [System.ComponentModel.DefaultValue(-1)]
        [System.ComponentModel.Description("Longitude of map or streetview")]
        public double Lng
        {
            get
            {
                return latLng.Lng;
            }
            set
            {
                if (double.IsNaN(value) || double.IsInfinity(value))
                {
                    log("Cannot set Lng, out of range " + value, LoggingFlags.ERRORS);
                    return;//throw new ArgumentOutOfRangeException("Lng", "Latitude must a valid numeric value: " + value);
                }
                while (value > 180)
                {
                    value -= 360;
                }
                while (value < -180)
                {
                    value += 360;
                }
                latLng = new LatLng(latLng.Lat, value);
                if (loaded)
                {
                    if (mapMode)
                    {
                        mapScriptInterop.panMapTo(latLng);
                    }
                    else
                    {
                        mapScriptInterop.setStreetViewPosition(latLng, viewSnapRadius);
                    }
                }
                else
                {
                    log("Cannot set Lng, loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        /// <summary>
        /// Set the latitude and longitude of the view in decimal degrees.
        /// 
        /// Note if you are changing the latitude and longitude use LatLng to avoid multiple the map loading twice.
        /// </summary>
        public LatLng LatLng
        {
            get
            {
                return latLng;
            }
            set
            {
                if (value == null || value.Lat > 90 || value.Lat < -90 || double.IsNaN(value.Lat) || double.IsInfinity(value.Lat) || double.IsNaN(value.Lng) || double.IsInfinity(value.Lng))
                {
                    return;
                }
                latLng = value;
                if (loaded)
                {
                    if (mapMode)
                    {
                        mapScriptInterop.panMapTo(latLng);
                    }
                    else
                    {
                        mapScriptInterop.setStreetViewPosition(latLng, viewSnapRadius);
                    }
                }
                else
                {
                    log("Cannot set LatLng loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        /// <summary>
        /// Determines if the map marker is shown.
        /// </summary>
        /// [System.ComponentModel.DefaultValue(false)]
        public bool MarkerShown
        {
            get
            {
                return markerShown;
            }
            set
            {
                markerShown = value;
                if (mapMode && loaded)
                {
                    mapScriptInterop.showMarker(value);
                }
                else
                {
                    log("Cannot set MarkerShown mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        /// <summary>
        /// Set the latitude and longitude of the marker in decimal degrees.
        /// </summary>
        public LatLng MarkerLatLng
        {
            get
            {
                return markerLatLng;
            }
            set
            {
                if (value == null || value.Lat > 90 || value.Lat < -90 || double.IsNaN(value.Lat) || double.IsInfinity(value.Lat) || double.IsNaN(value.Lng) || double.IsInfinity(value.Lng))
                {
                    return;
                }
                markerLatLng = value;
                if (mapMode && loaded)
                {
                    mapScriptInterop.moveMarker(markerLatLng);
                }
                else
                {
                    log("Cannot set MarkerLatLng mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        [System.ComponentModel.DefaultValue(8)]
        [System.ComponentModel.Description("Zoom factor of map")]
        public double MapZoom
        {
            get
            {
                if (!mapMode) return float.NaN;
                return mapZoom;
            }
            set
            {
                if (double.IsNaN(value) || double.IsInfinity(value))
                {
                    log("Cannot set MapZoom, out of range " + value, LoggingFlags.ERRORS);
                    return;//throw new ArgumentOutOfRangeException("MapZoom", "MapZoom must a valid numeric value: " + value);
                }
                mapZoom = value;
                if (mapMode && loaded)
                {
                    mapScriptInterop.setMapZoom(mapZoom);
                }
                else
                {
                    log("Cannot set MapZoom mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        [System.ComponentModel.Description("Map bounds")]
        public RectangularBounds MapBounds
        {
            get
            {
                if (!mapMode) return null;
                return mapBounds;
            }
            set
            {
                mapBounds = value;
                if (mapMode && loaded)
                {
                    mapScriptInterop.setMapBounds(mapBounds);
                }
                else
                {
                    log("Cannot set MapBounds mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        [System.ComponentModel.Description("Compass heading of streetview")]
        public double StreetViewHeading
        {
            get
            {
                if (mapMode) return float.NaN;
                return viewHeading;
            }
            set
            {
                if (double.IsNaN(value) || double.IsInfinity(value))
                {
                    log("Cannot set StreetViewHeading, out of range " + value, LoggingFlags.ERRORS);
                    return;//throw new ArgumentOutOfRangeException("StreetViewHeading", "StreetViewHeading must a valid numeric value: " + value);
                }
                while (value > 180)
                {
                    value -= 360;
                }
                while (value < -180)
                {
                    value += 360;
                }
                viewHeading = value;
                if (!mapMode && loaded)
                {
                    mapScriptInterop.setStreetViewPOV(viewHeading, viewPitch);
                }
                else
                {
                    log("Cannot set StreetViewHeading mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        [System.ComponentModel.Description("streetview Panorama")]
        public String StreetViewPano
        {
            get
            {
                if (mapMode) return "";
                return viewPano;
            }
            set
            {
                if (value == null)
                {
                    log("Cannot set StreetViewPano, requested pano string is null ", LoggingFlags.ERRORS);
                    return;
                }
                if (value.Length == 0)
                {
                    log("Cannot set StreetViewPano, requested pano is zero length string ", LoggingFlags.ERRORS);
                    return;
                }
                viewPano = value;
                if (!mapMode && loaded)
                {
                    mapScriptInterop.setStreetViewPano(viewPano);
                }
                else
                {
                    log("Cannot set StreetViewPano mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        [System.ComponentModel.Description("Pitch of elevation of streetview")]
        public double StreetViewPitch
        {
            get
            {
                if (mapMode) return float.NaN;
                return viewPitch;
            }
            set
            {
                if (value > 90 || value < -90 || double.IsNaN(value) || double.IsInfinity(value))
                {
                    log("Cannot set StreetViewPitch, out of range "+value, LoggingFlags.ERRORS);
                    return;//throw new ArgumentOutOfRangeException("StreetViewPitch", "StreetViewPitch must be between [-90,90], attempted value: " + value);
                }
                viewPitch = value;
                if (!mapMode && loaded)
                {
                    mapScriptInterop.setStreetViewPOV(viewHeading, viewPitch);
                }
                else
                {
                    log("Cannot set StreetViewPitch mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        [System.ComponentModel.Description("Zoom factor of streetview")]
        public double StreetViewZoom
        {
            get
            {
                if (mapMode) return float.NaN;
                return viewZoom;
            }
            set
            {
                if (double.IsNaN(value) || double.IsInfinity(value))
                {
                    log("Cannot set StreetViewZoom, out of range " + value, LoggingFlags.ERRORS);
                    return;//throw new ArgumentOutOfRangeException("StreetViewZoom", "StreetViewZoom must a valid numeric value: " + value);

                }
                viewZoom = value;
                if (!mapMode && loaded)
                {
                    mapScriptInterop.setStreetViewZoom(viewZoom);
                }
                else
                {
                    log("Cannot set StreetViewZoom mapMode=" + mapMode + " loaded=" + loaded, LoggingFlags.STRICT_ERRORS);
                }
            }
        }

        [System.ComponentModel.Description("Status of streetview")]
        public String StreetViewStatus
        {
            get
            {
                if (mapMode) return "MAP_MODE";
                return viewStatus;
            }
        }

        [System.ComponentModel.Description("Street view links")]
        public StreetViewLink[] StreetViewLinks
        {
            get
            {
                if (mapMode) return null;
                return links;
            }
        }

        [System.ComponentModel.Description("Last javascript error")]
        public String LastError
        {
            get
            {
                return lastError;
            }
        }

        [System.ComponentModel.Description("Map Control Version")]
        public String Version
        {
            get
            {
                return versionString;
            }
        }
    }
}
