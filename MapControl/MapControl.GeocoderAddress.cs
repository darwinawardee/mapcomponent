﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MappingControl
{
    public partial class MapControl : System.Windows.Forms.UserControl
    {
        public class GeocoderAddress
        {
            private String addressString;

            System.Collections.Generic.Dictionary<String, String[]> addressParts;

            public GeocoderAddress(String addressString)
            {
                this.addressString = addressString;
                addressParts = new System.Collections.Generic.Dictionary<string, string[]>();

                String[] addressComponents = addressString.Split(new char[] { ',' });

                foreach (String addressComponent in addressComponents)
                {
                    String[] componentParts = addressComponent.Split(new char[] { ',', '[', ']', ';' });
                    String value = componentParts[0];
                    for (int i = 1; i < componentParts.Length; i++)
                    {
                        if (addressParts.ContainsKey(componentParts[i]))
                        {
                            String[] arr = addressParts[componentParts[i]];
                            Array.Resize(ref arr, arr.Length + 1);
                            arr[arr.Length - 1] = value;
                            addressParts[componentParts[i]] = arr;
                        }
                        else
                        {
                            String[] arr = new String[] { value };
                            addressParts[componentParts[i]] = arr;
                        }
                    }

                }
            }

            public String[] getAvailableIds()
            {
                String[] result = new String[addressParts.Keys.Count];
                addressParts.Keys.CopyTo(result, 0);
                return result;
            }

            public String[] get(String id)
            {
                if (addressParts.ContainsKey(id))
                {
                    return addressParts[id];
                }
                else
                {
                    return new String[0];
                }
            }

            public String getNiceLocalString()
            {
                String result = "";
                String[] streetNo = get("street_number");
                String[] streetAdd = get("street_address");
                String[] route = get("route");
                String[] allParts = get("");
                if (streetNo.Length > 0)
                {
                    result += streetNo[0] + " ";
                }
                if (streetAdd.Length > 0)
                {
                    result += streetAdd[0] + " ";
                }
                else if (route.Length > 0)
                {
                    result += route[0] + " ";
                }

                if (result.Length < 1)
                {
                    if (allParts.Length > 0)
                    {
                        result += allParts[0];
                    }
                }
                if (result.Length < 1) result = "Address unknown";
                return result;
            }

            public String AddressString
            {
                get
                {
                    return addressString;
                }
            }
        }
    }
}
